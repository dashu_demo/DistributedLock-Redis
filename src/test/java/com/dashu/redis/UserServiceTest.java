package com.dashu.redis;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import com.dashu.redis.controller.model.User;
import com.dashu.redis.service.UserService;
import com.dashu.redis.utils.JedisPoolUtil;
import com.dashu.redis.utils.RedisLock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
	@Autowired
	private RedisLock disLock;
	@Autowired
	private JedisPoolUtil jedisPoolUtil;
	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	@Autowired
	private UserService userService;

	@Test
	public void saveUser() {
		User user = new User();
		user.setAge(0);
		user.setName("lisi");
		userService.saveUser(user);
	}

	@Test
	public void updateUser() {
		User user = new User();
		user.setId(1);
		user.setAge(10);
		user.setName("zhangsan");
		userService.saveUser(user);
	}

	@Test
	public void findAllUser() {
		System.out.println(userService.findAllUser());
	}

	@Test
	public void findUserById() {
		System.out.println(userService.findUserById(1));
	}

	@Test
	public void delUser() {
		userService.delUser(1);
	}

	/**
	 * 测试redis连接是否正常
	 */
	@Test
	public void test() {
		System.out.println(redisTemplate.opsForValue().get("hello"));
	}

	/**
	 * 30个并发 针对两个用户年龄的自增操作（记录锁）
	 */
	@Test
	public void testAddDisLock() {
		jedisPoolUtil.del("userTime");
		int total = 30;
		List<User> users = createUser(total);
		ExecutorService pool = Executors.newFixedThreadPool(total);
		final CountDownLatch latch = new CountDownLatch(total);

		for (int i = 0; i < total; i++) {
			User user = users.get(i);
			pool.submit(new Runnable() {
				@Override
				public void run() {
					try {
						latch.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					long start = System.currentTimeMillis();
					disLock.lock(String.valueOf(user.getId()), RedisLock.waitTime);
					// 非原子性操作
					User tmp = userService.findUserById(user.getId());
					tmp.setAge(tmp.getAge() + 1);
					userService.updateUser(tmp);
					disLock.unlock(String.valueOf(user.getId()));
					long end = System.currentTimeMillis();
					System.out.println("本次执行耗时：" + (end - start));
					System.out.println("并发数：" + total + ";当前总耗时：" + jedisPoolUtil.incrBy("userTime", (end - start)));
				}
			});
			latch.countDown();
		}
		try {
			latch.await();
			Thread.sleep(5000);
			System.out.println("所有线程执行完毕！");
			System.out.println("用户1执行操作后：" + userService.findUserById(users.get(0).getId()));
			System.out.println("用户2执行操作后：" + userService.findUserById(users.get(1).getId()));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		pool.shutdown();
		jedisPoolUtil.close();
	}

	/**
	 * 30个并发针对两个用户年龄的自增操作(操作锁)
	 */
	@Test
	public void testAddDisLock1() {
		jedisPoolUtil.del("userTime");
		int total = 30;
		List<User> users = createUser(total);
		ExecutorService pool = Executors.newFixedThreadPool(total);
		final CountDownLatch latch = new CountDownLatch(total);

		for (int i = 0; i < total; i++) {
			User user = users.get(i);
			pool.submit(new Runnable() {
				@Override
				public void run() {
					try {
						latch.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					long start = System.currentTimeMillis();
					disLock.lock();
					// 非原子性操作
					User tmp = userService.findUserById(user.getId());
					tmp.setAge(tmp.getAge() + 1);
					userService.updateUser(tmp);
					disLock.unlock();
					long end = System.currentTimeMillis();
					System.out.println("本次执行耗时：" + (end - start));
					System.out.println("并发数：" + total + ";当前总耗时：" + jedisPoolUtil.incrBy("userTime", (end - start)));
				}
			});
			latch.countDown();
		}
		try {
			latch.await();
			Thread.sleep(5000);
			System.out.println("所有线程执行完毕！");
			System.out.println("用户1执行操作后：" + userService.findUserById(users.get(0).getId()));
			System.out.println("用户2执行操作后：" + userService.findUserById(users.get(1).getId()));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		pool.shutdown();
		jedisPoolUtil.close();
	}

	/**
	 * 30个并发 针对两个用户年龄的自增操作(不加锁) 
	 */
	@Test
	public void testAdd() {
		jedisPoolUtil.del("userTime");
		int total = 30;
		List<User> users = createUser(total);
		ExecutorService pool = Executors.newFixedThreadPool(total);
		final CountDownLatch latch = new CountDownLatch(total);

		for (int i = 0; i < total; i++) {
			User user = users.get(i);
			pool.submit(new Runnable() {
				@Override
				public void run() {
					try {
						latch.await();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					long start = System.currentTimeMillis();
					// 非原子性操作
					User tmp = userService.findUserById(user.getId());
					tmp.setAge(tmp.getAge() + 1);
					userService.updateUser(tmp);
					long end = System.currentTimeMillis();
					System.out.println("本次执行耗时：" + (end - start));
					System.out.println("并发数：" + total + ";当前总耗时：" + jedisPoolUtil.incrBy("userTime", (end - start)));
				}
			});
			latch.countDown();
		}
		try {
			latch.await();
			Thread.sleep(5000);
			System.out.println("所有线程执行完毕！");
			System.out.println("用户1执行操作后：" + userService.findUserById(users.get(0).getId()));
			System.out.println("用户2执行操作后：" + userService.findUserById(users.get(1).getId()));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		pool.shutdown();
	}

	/**
	 * 根据并发数 构建用户数据集合 集合中只有两个用户
	 * 
	 * @param total
	 * @return
	 */
	private List<User> createUser(int total) {
		User zhangsan = userService.saveUser(new User("zhangsan", 0));
		User lisi = userService.saveUser(new User("lisi", 0));
		System.out.println("用户1：" + zhangsan + ";用户2：" + lisi + "均分并发数：" + total);
		List<User> users = new ArrayList<>();
		for (int i = 0; i < total; i++) {
			User user = new User();
			if (i % 2 == 0) {
				user.setId(zhangsan.getId());
			} else {
				user.setId(lisi.getId());
			}
			users.add(user);
		}
		return users;
	}
}
