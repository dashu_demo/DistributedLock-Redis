package com.dashu.redis.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dashu.redis.controller.model.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

}
