package com.dashu.redis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dashu.redis.controller.model.User;
import com.dashu.redis.dao.UserDao;
import com.dashu.redis.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public User saveUser(User user) {
		return userDao.save(user);
	}

	public User findUserById(int id) {
		return userDao.findById(id).get();
	}

	public List<User> findAllUser() {
		return userDao.findAll();
	}

	public User updateUser(User user) {
		return userDao.saveAndFlush(user);
	}

	public void delUser(int id) {
		userDao.deleteById(id);
	}
}
