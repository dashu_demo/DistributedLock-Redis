package com.dashu.redis.service;

import java.util.List;

import com.dashu.redis.controller.model.User;

public interface UserService {

	/**
	 * 保存用户信息
	 * 
	 * @param user
	 * @return
	 */
	public User saveUser(User user);

	/**
	 * 根据用户id查询用户信息
	 * 
	 * @param id
	 * @return
	 */
	public User findUserById(int id);

	/**
	 * 查询所有用户
	 * 
	 * @return
	 */
	public List<User> findAllUser();

	/**
	 * 更新用户信息
	 * 
	 * @param user
	 * @return
	 */
	public User updateUser(User user);

	/**
	 * 根据用户id删除用户
	 * 
	 * @param id
	 */
	public void delUser(int id);
}
