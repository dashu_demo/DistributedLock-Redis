package com.dashu.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dashu.redis.controller.model.User;
import com.dashu.redis.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;

	@RequestMapping("/find")
	public User findUser(int id) throws Exception {
		return userService.findUserById(id);
	}

	@RequestMapping("/save")
	public User saveUser(String name, Integer age) {
		User user = new User();
		user.setName(name);
		user.setAge(age);
		return userService.saveUser(user);
	}
}
