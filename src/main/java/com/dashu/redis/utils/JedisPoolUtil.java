package com.dashu.redis.utils;

import java.net.SocketTimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

@Component
public class JedisPoolUtil {

	private static final Logger logger = LoggerFactory.getLogger(JedisPoolUtil.class);
	@Autowired
	private JedisPool jedisPool;

	public void close() {
		jedisPool.close();
	}

	public Jedis getJedis() {
		int timeoutCount = 0;
		while (true) {
			try {
				if (null != jedisPool) {
					return jedisPool.getResource();
				}
			} catch (Exception e) {
				if (e instanceof JedisConnectionException || e instanceof SocketTimeoutException) {
					timeoutCount++;
					logger.warn("getJedis timeoutCount={}", timeoutCount, e);
					if (timeoutCount > 3) {
						logger.error("获取jedis实例次数超过3次仍未获取到", e);
						break;
					}
				} else {
					logger.warn("jedisInfo ... NumActive=" + jedisPool.getNumActive() + ", NumIdle="
							+ jedisPool.getNumIdle() + ", NumWaiters=" + jedisPool.getNumWaiters() + ", isClosed="
							+ jedisPool.isClosed());
					logger.error("GetJedis error,", e);
					break;
				}
			}
			break;
		}
		return null;
	}

	public String luaExistKeyDel(String key, String uuid) {
		String result = "0";
		Jedis jedis = getJedis();
		if (jedis == null) {
			logger.error("获取redis连接失败");
			return result;
		}
		final String script = "if redis.call('get',KEYS[1]) == ARGV[1]"
				+ "then  return redis.call('del', KEYS[1])  else " + "return 0 end ";
		Object redisResult = null;
		try {
			redisResult = jedis.eval(script, 1, key, uuid);
		} catch (Exception e) {
			logger.error("解锁操作执行redis的lua脚本命令失败", e);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
		if (redisResult == null) {
			return result;
		}
		if ("1".equals(String.valueOf(redisResult))) {
			result = "1";
		}
		return result;
	}

	public void set(String key, String value) {
		Jedis jedis = getJedis();
		if (jedis == null) {
			logger.error("获取redis连接失败");
			return;
		}
		try {
			jedis.set(key, value);
		} catch (Exception e) {
			logger.error("执行redis中set命令失败", e);
		} finally {
			if (jedis != null)
				jedis.close();
		}
	}

	public Long incrBy(String key, long step) {
		Jedis jedis = getJedis();
		if (jedis == null) {
			logger.error("获取redis连接失败");
			return -1L;
		}
		try {
			return jedis.incrBy(key, step);
		} catch (Exception e) {
			logger.error("执行redis中incrBy命令失败", e);
		} finally {
			if (jedis != null)
				jedis.close();
		}
		return -1L;
	}

	public Long del(String key) {
		Jedis jedis = getJedis();
		if (jedis == null) {
			logger.error("获取redis连接失败");
			return 0L;
		}
		try {
			return jedis.del(key);
		} catch (Exception e) {
			logger.error("执行redis中del命令失败", e);
		} finally {
			if (jedis != null)
				jedis.close();
		}
		return 0L;
	}
}
