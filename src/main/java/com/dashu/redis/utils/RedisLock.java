package com.dashu.redis.utils;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

@Component
public class RedisLock implements Lock {
	public static final int waitTime = 10;
	private static final Logger logger = LoggerFactory.getLogger(RedisLock.class);
	private static ThreadLocal<String> tokenMap = new ThreadLocal<>();
	private static final Random rm = new Random();
	@Autowired
	private JedisPoolUtil jedisPoolUtil;

	/**
	 * 加锁操作
	 * 
	 * @param lock
	 * @param waitTime
	 */
	public void lock(String lock, int waitTime) {
		if (tryLock(lock)) {
			return;
		} else {
			try {
				Thread.sleep(rm.nextInt(waitTime));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			lock(lock, waitTime);
		}
	}

	/**
	 * 尝试加锁操作
	 * 
	 * @param lock
	 * @return
	 */
	public boolean tryLock(String lock) {
		String uuid = UUID.randomUUID().toString();
		String setRedisLock = "";
		Jedis jedis = jedisPoolUtil.getJedis();
		if (jedis == null) {
			logger.error("获取jedis实例失败");
			return false;
		}
		try {
			setRedisLock = jedis.set(lock, uuid, "NX", "EX", 200);
		} catch (Exception e) {
			logger.error("执行redis加锁" + lock + "操作命令失败", e);
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
		if ("OK".equalsIgnoreCase(setRedisLock)) {
			logger.warn("加锁" + lock + "成功");
			tokenMap.set(uuid);
			return true;
		} else {
			logger.warn("尝试加锁" + lock + "失败");
		}
		return false;
	}

	/**
	 * 解锁操作
	 * 
	 * @param lock
	 */
	public void unlock(String lock) {
		String uuid = tokenMap.get();
		String luaExistKeyDel = jedisPoolUtil.luaExistKeyDel(lock, uuid);
		if ("1".equals(luaExistKeyDel)) {
			logger.warn("解锁" + lock + "成功");
			tokenMap.remove();
		} else if ("0".equals(luaExistKeyDel)) {
			logger.warn("解锁" + lock + "失败");
		}
	}

	@Override
	public void lock() {
		lock("lock", waitTime);
	}

	@Override
	public boolean tryLock() {
		return tryLock("lock");
	}

	@Override
	public void unlock() {
		unlock("lock");
	}

	@Override
	public Condition newCondition() {
		return null;
	}

	@Override
	public void lockInterruptibly() throws InterruptedException {

	}

	@Override
	public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
		return false;
	}
}
