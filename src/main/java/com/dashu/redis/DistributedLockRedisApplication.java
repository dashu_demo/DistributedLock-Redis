package com.dashu.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
@EnableCaching
@SpringBootApplication
public class DistributedLockRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistributedLockRedisApplication.class, args);
	}
}
